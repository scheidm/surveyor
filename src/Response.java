import java.util.ArrayList;

public class Response implements java.io.Serializable
{
	// Response and options implement match method to create simple grading structure. If match returns true then give points for the problem.
	// Responses are the same if they have the same options in the same order
	// Multi-choice and Matching entries will sort the options before storage for the correct answer
	// Ranking will not as order matters
	private static final long serialVersionUID = -8232099408579288491L;
	private ArrayList<Option> options_;
	private transient Input i_;
	private Boolean graded_;
	
	public ArrayList<Option> get_options()
	{
		return options_;
	}
	public void set_options(ArrayList<Option> l)
	{
	
	}
	public void add_option(Option o){
		options_.add(o);
	}
	public Boolean match(Response r)
	{
		ArrayList<Option> compare = r.get_options();
		int count=0;
		for (Option u: compare){
			if (!u.match(options_.get(count))){return false;}
			count+=1;
		}
		return true;
	}
	public Response(Input i){
		set_inp(i);
		options_=new ArrayList<Option>();
		graded_=true;
	}

	public void display(Output out) {
		for(Option o:options_){
			o.display(out);
			out.out("\n");
		}
		
	}
	public Input get_inp() {
		return i_;
	}
	
	public void set_inp(Input i_) {
		this.i_ = i_;
	}
	public Boolean get_graded() {
		return graded_;
	}
	public void set_graded(Boolean graded_) {
		this.graded_ = graded_;
	}
	
}
