
public class FeatureFactory {	
	private Renderer gfx;
	public FeatureFactory(Renderer rend){ this.gfx=rend;}
	public Renderer getRenderer(){ return gfx; }
	public Geometry createGeometry(int width, int height ){
		return new Geometry( width, height );
	}
	public Point createPoint(int x, int y) {
		return new Point(gfx,x, y);
	}
	public Rectangle createRectangle(int x1, int y1, int x2, int y2) {
		return new Rectangle(gfx, x1,y1,x2,y2);
	}
	public Circle createCircle(int x, int y, int radius) {
		return new Circle(gfx, x, y, radius);
	}
	public Polygon createPolygon() {
		return new Polygon(gfx);
	}
	public Text createText(int x, int y, String value) {
		return new Text(gfx, x, y, value);
	}
	public Text createText(int x, int y, String value, int size) {
		return new Text(gfx,x, y, value, size);
	}
}

