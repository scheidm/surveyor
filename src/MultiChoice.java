import java.util.ArrayList;

public class MultiChoice extends Question
{
	private static final long serialVersionUID = -4464974716574520871L;
	protected ArrayList<Option> options_;
	
	public MultiChoice(){options_= new ArrayList<Option>();}
	public MultiChoice( Output o, Input in ){
		super(o, in);
		options_= new ArrayList<Option>();
	}

	public ArrayList<Option> inputVal(){
		return i_.matchX(options_,false);
	}
	
	public Response response(){
		display();
		Response r=new Response(i_);
		for (Option o: inputVal()){
			r.add_option(o);
		}
		r.set_graded(true);
		return r;
	}
	public void display(){
		o_.out(prompt_+"\n");
		o_.out("Select the correct option(s) below\n");
		Integer c=1;
		for( Option i:options_){
			o_.out(c.toString()+": ");
			i.display(o_);
			o_.out("\n");
			c+=1;
		}
		o_.out("\n");
	}
	public ArrayList<Option> get_option(){ return options_;	}
	
	public void set_option(ArrayList<Option> o){	options_=o; }
	
	public void set_option(){
		while(true){
			o_.out("Modify which item?\n");
			Integer c=1;
			o_.out("0: Finished modifying\n");
			for( Option i:options_){
				o_.out(c.toString()+": ");
				i.display(o_);
				o_.out("\n");
				c+=1;
			}
			Option cur=new StringOpt(o_);
			Integer x=i_.IntInRange(0,c-1);
			cur=options_.get(x-1);
			cur.build(i_, o_);
			return;
		}
	}		

	public void add_option(Option o){options_.add(o);}
	
	public void add_option(){
		Option opt = new StringOpt(o_);
		opt.build(i_, o_);
		options_.add(opt);
	}
	
	@Override
	public void build() {
		super.build();
		while(true){
			add_option();
			o_.out("Add another option? Y/N\n");
			if ( !i_.YesNo()){return;}
		}
	}
	
	public void modify(){
		super.modify();
		boolean done=false;
		o_.out("Modify options? Y/N\n");
		if (i_.YesNo()){
			while( !done ){
				modify_option(options_);
				o_.out("Modify another option? Y/N\n");
				if(!i_.YesNo()){done=true;	}
			}
		}
		done=false;
		o_.out("Add options? Y/N\n");
		if (i_.YesNo()){
			while( !done ){
				add_option();
				o_.out("Add another option? Y/N\n");
				if(!i_.YesNo()){done=true;	}
			}
		}
	}
	public void modify_option(ArrayList<Option> opts){
		o_.out("Modify which option?\n");
		int count=1;
		for(Option o:opts){
			o_.out(count+": ");
			o.display(o_);
			o_.out("\n");
			count+=1;				
		}
		int inp=i_.IntInRange(1, count);
		opts.get(inp-1).modify(i_, o_);
	}

	public void tab_output(ArrayList<Pair> counted){
		int[] counter=new int[options_.size()];
		int count=0;
		for (Pair p: counted){
			int change=p.count;
			for (Option u: p.r.get_options()){
				for(Option o:options_ ){
					if(u.equals(o)){
						counter[count]+=change;
						break;
					}
					count+=1;
				}
			count=0;
			}
		}	
		for ( Option o: options_){
			o.display(o_);
			o_.out(": "+counter[count]+"\n");
			count+=1;
		}

	}
	public void tabulate(ArrayList<Response> inputs ){
		ArrayList<Pair> unique = new ArrayList<Pair>();//stores counts with each response
		for (Response r:inputs){
			boolean newR=true;
			for(Pair c:unique){
				if (c.r.match(r))
				{
					newR=false;
					c.count+=1;
				}
			}
			if (newR){
				Pair p=new Pair(r);
				unique.add(p);
			}
		}
		tab_output(unique);
	}
}
