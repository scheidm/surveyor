import sys

class makeClass():
	def __init__(self, name):
		self.name=name
		self.dec="public class "+name
		self.var=[]
		self.method=[]
		self.source=""
		self.notes=[]
	
	def printMe(self):
		self.source.write( self.dec )
		self.source.write( "\n{\n\n" )
		for i in self.notes:
			self.source.write(i)
		for i in self.var:
			self.source.write(i)
		self.source.write("\n")
		for i in self.method:
			self.source.write(i)
		self.source.write("\n}\n")
	

def inherits( match, class2 ):
	match.dec=match.dec+" extends "+class2

def contains( match, class2 ):
	match.var.append("	private "+class2+" #####;\n")

def note( match, class2 ):
	x=class2.split(':')
	match.notes.append("// "+x[1]+"\n")


varScope={'+':"public ", '-':"private ", '#':"protected "}
cRel={ "^--":"inherits(match,class2)", '<>-':'contains(match,class2)',
'---':'note(match,class2)'} 
inp=open( sys.argv[1] )
l=inp.readline()
toClose=[ inp ] 
eClasses=[]
count=0
while l:
	print count
	if( len(l)==l.find("]")+2 ):
		c1var=[]
		c1method=[]
		c1=l[1:-2]
		class1=c1.split('|')
		print "create "+class1[0]
		c1var=class1[1].split(';')
		if( len(class1)>2 ):
			c1method=class1[2].split(';')
	 	temp=makeClass( class1[0] )
		if c1var[0]=='':
			c1var=[]
		for i in c1var:
			z=i.split(':')
			temp.var.append("	"+varScope[ i[0] ]+z[1]+" "+z[0][1:]+"_;\n")
		for i in c1method:
			z=i.split(':')
			temp.method.append("	"+varScope[ i[0] ]+z[1]+" "+z[0][1:]+"\n	{\n	\n	}\n")
		tFile=open( class1[0]+".java",'w' )
		temp.source=tFile
		eClasses.append( temp )
		toClose.append(tFile)
	else:
		match=None
		x=l.find(']')
		c1 = l[1:x]
		for i in eClasses:
			if i.name==c1:
				match=i
		rel=l[x+1:x+4]
		class2=l[x+5:-2]
		print c1+rel+class2
		exec( cRel[ rel ] )
		print
	l=inp.readline()
	count+=1

for i in eClasses:
	i.printMe()
for i in toClose:
	i.close()

