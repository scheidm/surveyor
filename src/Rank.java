import java.util.ArrayList;

public class Rank extends MultiChoice
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7673359165542640265L;
	public Rank()
	{
	
	}
	public Rank(Output o, Input i){
		super(o,i);
	}

	public void build() {
			super.build();
	}
	public void display(){
		o_.out("Put the following in order, separated by commas ( ex. 3,1,2 )\n");
		o_.out(prompt_+"\n");
		Integer c=1;
		for( Option i:options_){
			o_.out(c.toString()+": ");
			i.display(o_);
			o_.out("\n");
			c+=1;
		}
	}
	public void modify(){
		int size=options_.size();
		super.modify();
		if(size!=options_.size()&&correct_!=null){set_correct(response());}
	}
	public void tab_output(ArrayList<Pair> counted){
		for (Pair p:counted){
			p.r.display(o_);
			o_.out("Submitted "+p.count+" time(s)\n\n");
		}
	}

	public ArrayList<Option> inputVal(){
		return i_.matchX(options_,true);
	}
}
