import java.util.ArrayList;

public class Polygon extends Feature {
	private ArrayList<Point> points;
	public Polygon( Renderer rend, final Point... pts ){
		this.gfx=rend;
		this.points=new ArrayList<Point>();
		for (Point p: pts){
			this.points.add(p);
		}
		
	}
	public int size(){return 0;}
	public Point get(int i){return points.get(i);}
	public void set(int i, Point p){ points.set(i, p);}
	
	public void add(int x, int y){points.add(new Point(gfx,x,y));}
	public void add(int i, int x, int y){points.add(i, new Point(gfx,x,y));}
	public void add(int i, Point p){points.add(i,p);}
	public void add(Point p){points.add(p);}
	public void remove(int i){points.remove(i);}
	public void remove(Point p){points.remove(p);}
	@Override
	public void translate(int dx, int dy) {
		// TODO Auto-generated method stub

	}

	@Override
	public void render() {
		Point a=null;
		Point b=null;
		for(int i=0; i<points.size()-1; i++){
			a=points.get(i);
			b=points.get(i+1);
			gfx.drawLine(a.getX(),a.getY(),b.getX(),b.getY());
		}
		a=points.get(0);
		b=points.get(points.size()-1);
		gfx.drawLine(a.getX(),a.getY(),b.getX(),b.getY());
	}

}
