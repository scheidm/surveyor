import java.util.ArrayList;

public class ShortW extends Essay
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2888737385662763756L;

	public ShortW(Output o, Input i) {
		o_=o;
		i_=i;
	}

	public void display(){
		o_.out("Respond to the below in 10 words or less\n");
		o_.out(prompt_+"\n");
	}
	public void tabulate(ArrayList<Response> inputs ){
		ArrayList<Pair> unique = new ArrayList<Pair>();//stores counts with each response
		for (Response r:inputs){
			boolean newR=true;
			for(Pair c:unique){
				if (c.r.match(r))
				{
					newR=false;
					c.count+=1;
				}
			}
			if (newR){
				Pair p=new Pair(r);
				unique.add(p);
						
			}
		}
		tab_output(unique);
	}	
	public Response response(){
		display();
		Option o=new StringOpt(o_);
		o.set_text(i_.inStr());
		Response r=new Response(i_);
		r.add_option(o);
		return r;	
	}
	public void tab_output(ArrayList<Pair> counted){
		for (Pair p:counted){
			p.r.display(o_);
			o_.out("Submitted "+p.count+" time(s)\n\n");
		}
	}
}
