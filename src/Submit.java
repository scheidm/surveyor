import java.util.ArrayList;

public class Submit implements java.io.Serializable
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -587749875905969237L;
	private ArrayList<Response> responses_;
	private double grade_;
	private String user_;
	public ArrayList<Response> get_responses()
	{
		return responses_;
	}
	public void set_responses(ArrayList<Response> l)
	{
		responses_=l;
	}
	public void display(Output o){
		int count=1;
		for (Response r:responses_){
			o.out("Question #"+count+" response:\n");
			r.display(o);
		}
	}
	public void display(Output o, int i){
		responses_.get(i).display(o);
	}
	public void add_response(Response r){
		responses_.add(r);
	}
	public Submit()
	{
		responses_=new ArrayList<Response>();
		set_grade(0);
		set_user("");
	}
	public double get_grade() {
		return grade_;
	}
	public void set_grade(double grade) {
		this.grade_ = grade;
	}
	public String get_user() {
		return user_;
	}
	public void set_user(String user) {
		this.user_ = user;
	}

}
