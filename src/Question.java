import java.util.ArrayList;

public abstract class Question implements java.io.Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8677745254463874878L;
	protected Output o_;
	protected String prompt_;
	protected ArrayList<String> responseType_;
	protected transient Input i_;
	protected Response correct_;
	protected Boolean graded_;
	
	public abstract void display();
	
	public void modify(){
		o_.out("Modify prompt? Y/N\n");
		if (i_.YesNo()){
			o_.out("Current Prompt:\n");
			o_.out(prompt_+"\n");
			set_prompt();
			
		}
	}
	public void tabulate(ArrayList<Response> inputs){
		for (Response cur: inputs){
			cur.display(o_);
			o_.out("\n");
		}
	}
	public void set_prompt(String s){ prompt_=s; }
	
	public void set_prompt(){
		o_.out("Enter question prompt\n");
		prompt_=i_.inStr();
	}
	
	public void showCorrect(){
		correct_.display(o_);
	}
	public String get_prompt() { return prompt_; }
	
	public Output get_out(){ return o_; }
	
	public void set_out(Output o){ o_=o; }
	
	public Input get_in(){ return i_; }
	
	public void set_in(Input in){ i_=in; }

	public boolean grade(Response r){
		if (r.match(correct_)){ return true;}
		else {return false;}
	}
	
	public void set_out(Input i){ i_=i; }

	public Question(){
		prompt_="";
		responseType_= new ArrayList<String>();
	}
	
	public Response get_correct() {
		return correct_;
	}
	public abstract Response response();
	public void set_correct(Response correct_) {
		this.correct_ = correct_;
	}

	public void build(){
		set_prompt();
	}
	
	public Question(Output o, Input i)
	{
		o_=o;
		i_ = i;
		responseType_= new ArrayList<String>();
		prompt_="";
	}
	
}
