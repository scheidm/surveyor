import java.util.ArrayList;
import java.util.Scanner;


public class Cin extends Input {
	
	/**
	 * 
	 */
	private Scanner s_;
	private Output o_;
	
	public String inStr(){
		while (true){
			try {
				return s_.nextLine();
				} catch (Exception e) {
				o_.out("Invalid input");
			}
		}
	
	}
	public int inInt(){
		while (true){
			String x=this.inStr();
			x=x.replace("\n","").replace("\r", "");
			try {
				Integer r = Integer.parseInt(x);
				return r;
			} catch (Exception e) {
				o_.out("Invalid input\n");
			}
		}
	}
	
	public Cin(Output o){
		s_=new Scanner(System.in);
		o_=o;
	}
	
	public boolean YesNo(){
		while(true){
			String r=inStr();
				if ( r.equals("N")||r.equals("n") ){
					return false;
				}
				else if (r.equals("Y")||r.equals("y")){
					return true;
				}
				else{
					o_.out("Invalid input\n");
				}
			}
		}
	public boolean ValidInt(int lo, int hi){
		while(true){
			int i=inInt();
			if (i>=lo&&i<=hi){
				return true;
			}
			else{return false;}
		}
	}
	
	public int IntInRange(int lo, int hi){
		while(true){
			int i=inInt();
			if (i>=lo&&i<=hi){
				return i;
			}
			else{o_.out("Invalid input\n");
			}
		}
	}
	
	public ArrayList<Option> matchX(ArrayList<Option> opts, boolean rank){
		Integer cur=0;
		int size=opts.size();
		boolean done=false;
		boolean[] pick = new boolean[size];
		ArrayList<Option> answer=new ArrayList<Option>();
		while( !done ){
			String inp=inStr();	//capture input
			inp.replace(" ","");
			String[] l=inp.split(",");//validate input
			if(rank&&l.length!=size){o_.out("Must rank all items\n");}
			else{
				for(String s:l){
					try {
						cur=Integer.parseInt(s);
					} catch (NumberFormatException e) {//not integers
						o_.out("invalid entry\n");
						done=false;
						answer=new ArrayList<Option>();
						break;
					}
					if(cur>size||cur<0){//not valid index
						o_.out("invalid entry\n");
						done=false;
						answer=new ArrayList<Option>();
						break;
					}
					
					else if( pick[cur-1] ){
						o_.out("invalid entry, once per item\n");
						done=false;
						answer=new ArrayList<Option>();
						break;
					}
					else if( pick[cur-1] ){//already chosen
						o_.out("invalid entry, once per item\n");
						done=false;
						answer=new ArrayList<Option>();
						break;
					}
					else
					{
						answer.add(opts.get(cur-1));
						pick[cur-1]=true;
						done=true;
					}
					
				}
				if(done==false){//failed input, reset
					pick=new boolean[size];
				}
			}
		}
		if (!rank){
			answer=new ArrayList<Option>();
			int count=0;
			for (boolean b:pick){
				if (b){	answer.add(opts.get(count));}
				count+=1;
			}
		}
		return answer;
	}
}
