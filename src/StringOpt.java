public class StringOpt extends Option
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3754133720750663849L;


	public StringOpt(Output o)
	{
		super(o);
	}
	
	public StringOpt(){}

	@Override
	public void build(Input in, Output o) {
		o.out("Enter new text\n");
		textVal_=in.inStr();
	}
	
	public void modify(Input in, Output o){
		o.out(textVal_+"\n");
		build(in, o);
	}

	public boolean match(Option o){
		String s=o.get_text();
		return textVal_.equals(s);
	}
	
	

}
