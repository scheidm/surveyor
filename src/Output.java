
public abstract class Output implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3846733875580378731L;
	public abstract void out( String s );
	public abstract void out( StringOpt s);
	public abstract void out( Integer i );
	public abstract void out(char x);

	}
