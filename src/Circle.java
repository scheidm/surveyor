
public class Circle extends Feature {
	int x;
	int y;
	int radius;
	
	public Circle( Renderer rend, int x, int y, int radius){
		this.gfx=rend;
		this.x=x;
		this.y=y;
		this.radius=radius;
	}
	
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	@Override
	public void translate(int dx, int dy) {
		// TODO Auto-generated method stub

	}

	@Override
	public void render() {
		gfx.drawCircle(x,y,radius);
	}

}
