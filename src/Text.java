
public class Text extends Feature {
	private String text;
	private int x;
	private int y;
	private int size;
	public static final int DEFAULT_SIZE=12;
		
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public Text(Renderer rend, int x, int y, String txt){
		this.text=txt;
		this.gfx=rend;
		this.x=x;
		this.y=y;
		this.size=DEFAULT_SIZE;
	}
	
	public Text(Renderer rend, int x, int y, String txt,int size){
		this.text=txt;
		this.gfx=rend;
		this.x=x;
		this.y=y;
		this.size=size;		
	}

	@Override
	public void translate(int dx, int dy) {
		// TODO Auto-generated method stub

	}

	@Override
	public void render() {
		gfx.drawText(x, y, text, size);
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
