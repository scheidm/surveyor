
public class Essay extends Question
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7337838603054539539L;

	public Essay(Output o, Input i) {
		o_=o;
		i_=i;
	}	
	
	public void display(){
		o_.out("Respond to the below question in detail\n");
		o_.out(prompt_+"\n");
	}
	public Essay(){};
	
	public Response response(){
		display();
		Response r=new Response(i_);
		r.set_graded(false);
		Option o=new StringOpt(o_);
		o.build(i_, o_);
		r.add_option(o);
		return r;	
	}
}
