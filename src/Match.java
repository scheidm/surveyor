
import java.util.ArrayList;

public class Match extends MultiChoice
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6828266570313563059L;
	protected ArrayList<Option> matches_;

	public void display()
	{
		super.display();
		o_.out("Match to:\n");
		char c=65;
		for(Option o:matches_){
			o_.out(c+": ");
			o.display(o_);
			o_.out("\n");
			c+=1;
		}
		o_.out("\n");
	}
	public void set_matches(ArrayList<Option> l)
	{
		
	}
	public ArrayList<Option> get_matches()
	{
		return matches_;
	
	}
	public void add_option()
	{
		super.add_option();
		o_.out("Add matching option\n");
		add_match();
	}
	public void add_match(Option m)
	{
	
	}
	public void add_match()
	{
		Option opt = new StringOpt(o_);
		opt.build(i_, o_);
		matches_.add(opt);
	}
	public void save( String file )
	{
	
	}
	public void load( String file )
	{
	
	}
	public Match()
	{
		options_= new ArrayList<Option>();
		matches_= new ArrayList<Option>();
	}
	public Match(Output o, Input i) {
		o_=o;
		i_=i;
		options_= new ArrayList<Option>();
		matches_= new ArrayList<Option>();
	}
	@Override
	public void build(){
		super.build();
		while(true){
			o_.out("Add another match? Y/N\n");
			if ( !i_.YesNo() ){ 
				java.util.Collections.shuffle(matches_);
				o_.out("Matches shuffled\n");
				return;
			}
			else{add_match();}
		}
				
	}

	public void tab_output(ArrayList<Pair> counted){
		for (Pair p:counted){
			p.r.display(o_);
			o_.out("Submitted "+p.count+" time(s)\n\n");
		}
	}
	
	public void modify(){
		int msize=matches_.size();
		super.modify();
		boolean done=false;
		o_.out("Modify matches? Y/N\n");
		if (i_.YesNo()){
			while( !done ){
				modify_option(matches_);
				o_.out("Modify another match? Y/N\n");
				if(!i_.YesNo()){done=true;	}
			}
		}
		done=false;
		o_.out("Add matches? Y/N\n");
		if (i_.YesNo()){
			while( !done ){
				add_match();
				o_.out("Add another match? Y/N\n");
				if(!i_.YesNo()){done=true;	}
			}
		}
		if(msize!=matches_.size()){
			java.util.Collections.shuffle(matches_);
			o_.out("Matches shuffled\n");
			if(correct_!=null){set_correct(response());}
			}
	}
	
	public Response response(){
		Response r=new Response(i_);
		ArrayList<Option> unused = new ArrayList<Option>(matches_);
		o_.out("Select the matches for the following:\n");
		for(Option o:options_){
			o_.out("Select the match for: ");
			o.display(o_);
			o_.out("\n");
			int count=1;
			for(Option j:unused){
				o_.out(count+": ");
				j.display(o_);
				o_.out("\n");
				count+=1;
			}
			int inp=i_.IntInRange(1, unused.size());
			PairOption cur=new PairOption();
			cur.set_left(o);
			cur.set_right(unused.get(inp-1) );
			r.add_option((Option)cur);
			unused.remove(inp-1);
		}
		return r;
	}
}
