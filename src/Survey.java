import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class Survey implements java.io.Serializable
{

// submissions is a list pointing to submissions stored in users
// Tabulate maps Response to count for each response received. Essays are saved in the list for reference

	private static final long serialVersionUID = -3237640702948698779L;
	protected ArrayList<Question> questions_;
	protected ArrayList<Submit> submissions_;
	protected String name_;
	protected Output o_;
	protected transient Input i_;
	
	public Survey(Output o, Input i) {
		o_=o;
		i_=i;
		name_="";
		questions_=new ArrayList<Question>();
		submissions_=new ArrayList<Submit>();

	}

	public void administer(){
		Submit s=new Submit();
		o_.out("Now taking "+this.getClass().getName()+": "+name_+"\n");
		int count=1;
		for (Question q: questions_){
			o_.out("Question #"+count+":\n");
			s.add_response(q.response());
			count+=1;
		}
		submissions_.add(s);
		save();
		o_.out("\nSubmission completed and filed\n\n");
		menu();
	}
public void display()//menus self-contained, displays then returns to menu
{
		o_.out(name_+"\n");
		for (Question q: questions_){
			q.display();
			o_.out("\n");
		}
		o_.out("\n");
	}
	public void save()
	{
		try
	      {
			 FileOutputStream fileOut = 
	         new FileOutputStream(name_+".s.ser");
	         ObjectOutputStream out = new ObjectOutputStream(fileOut);
	         out.writeObject(this);
	         out.close();
	          fileOut.close();
	      }catch(IOException i)
	      {
	          i.printStackTrace();
	      }
	}
	
	public void tabulate(){
		int count=0;
		ArrayList< ArrayList<Response> > aggregate = new ArrayList<ArrayList<Response>>();
		for (int i = 0; i < questions_.size(); i++) {
			aggregate.add( new ArrayList<Response>());
		}
		for (Submit s: submissions_){
			 ArrayList<Response> cur= s.get_responses();
			 for (Response r: cur){
				 aggregate.get(count).add(r);
				 count+=1;
			 }
			 count=0;
		}
		for(Question q: questions_){
			o_.out("\nQuestion #"+(count+1)+":\n");
			o_.out(q.get_prompt()+"\nResponses:\n\n");
			q.tabulate(aggregate.get(count));
			count+=1;
		}
		menu();
	}
	public void showStats(){}//Stub
	public ArrayList<Submit> get_submissions()
	{
		return submissions_;
	}
	public void set_submissions(ArrayList<Submit> l)
	{
	
	}
	public void add_submission( Submit s)
	{
		submissions_.add(s);
	}
	public ArrayList<Question> get_questions()
	{
		return questions_;
	}
	public void set_questions(ArrayList<Question> l)
	{
		questions_=l;
	}
	public void pickq(){
		o_.out("Pick a question type\n");
		o_.out(" 1: Multiple Choice\n 2: True/False\n");
		o_.out(" 3: Matching\n 4: Ranking\n");
		o_.out(" 5: Short Answer\n 6: Essay\n");
	}
	public void add_question(){ //Question generation menu
		pickq();
		Question x=null;
		int inp=i_.IntInRange(1,6);
		switch(inp){
		case 1:	x=new MultiChoice(o_,i_);
			o_.out("Create Multiple Choice question\n");
			x.build();
			add_question(x);
			break;
		case 2: x=new BoolQ(o_,i_);
			o_.out("Create True/False question\n");
			x.build();
			add_question(x);
			break;
		case 3:	x=new Match(o_,i_);
			o_.out("Create Matching question\n");
			x.build();
			add_question(x); 
			break;
		case 4: x=new Rank(o_,i_);
			o_.out("Create Ranking question\n");
			x.build();
			add_question(x);
			break;
		case 5: x=new ShortW(o_,i_);
			o_.out("Create Short Answer question\n");
			x.build();
			add_question(x);
			break;
		case 6: x=new Essay(o_,i_);
			o_.out("Create Essay question\n");
			x.build();
			add_question(x);
			break;
		}
	
	}
	public void add_question(Question q) {
		questions_.add(q);
	}
	public void set_name(String n){ name_=n;}
	
	public void set_name(){
		o_.out("Enter name\n");
		Boolean done=false;
		while (done==false){
			name_=i_.inStr();
			if (name_.equals("")){
				o_.out("Invalid entry\n");
			}
			else{
				done=true;
			}
		}
	}

	public void view_responses(){
		int cur=1;
		for(Submit s:submissions_){
			o_.out("Submission #"+cur+":\n");	
			for (int i=0; i<questions_.size();i++){
				o_.out("Question #"+(i+1)+"\n");
				o_.out("Prompt: "+questions_.get(i).get_prompt()+"\n");
				s.display(o_,i);
				o_.out("\n");
			}
			cur+=1;
		}
		menu();
	}
	public void menu(){//Menu driver for existing surveys
		o_.out(name_+" main menu\n");
		o_.out("-1: Exit without saving\n 0: Save and exit\n");
		o_.out(" 1: Display\n 2: Modify\n");
		o_.out(" 3: Administer\n 4: Display responses\n");
		o_.out(" 5: Tabulate\n 6: Print Survey\n");
		
		int r = i_.IntInRange(-1, 6);
		switch(r){
		case 6:
				printme(300);
				break;
		case 5:
				tabulate();
				break;
		case 4:
			view_responses();
			break;
		case 3: administer();
			break;
		case 2: modify();
			this.menu();
			break;
		case 1:	this.display();
			this.menu();
			break;
		case 0: this.save(); return;
		case -1: return;
		}
		
	}
	public void printme(int perQ) {
		Output old=o_;
		XmlOut xout=new XmlOut(this.name_+".xml");
		int h=20+perQ*questions_.size();
		if(h>1500){h=1500;}
		xout.header(h);
		xout.openText();
		o_=(Output)xout;
		for (Question q:questions_){
			q.set_out((Output)xout);
		}
		display();
		xout.closeText();
		xout.footer();
		for (Question q:questions_){
			q.set_out(old);
		}
		xout.close();
		o_=old;
		o_.out("Print in what format?\n1: PostScript\n2: PNG image\n");
		int s=i_.IntInRange(1, 2);
		String[] genArgs={"-ps",(name_+".xml"),(name_+".ps")};
		switch(s){
		case 2:
			genArgs[0]="-png";
			genArgs[2]=(name_+".png");
		case 1:
			Export.main(genArgs);
			break;
			
		}
		menu();
	}

	public void build() {//checks name_ to see if new object, otherwise prompt-driven modify(unimplemented)
		set_name();
		o_.out("Add Questions:\n");
		while(true){
			add_question();
			o_.out("Add another question? Y/N\n");
			if ( !i_.YesNo() ) {
				return;	}
		}
	}
	
	public void modify(){
		boolean done=false;
		o_.out("Modify name? Y/N\n");
		if (i_.YesNo()){
			o_.out("Current namet:\n");
			o_.out(name_+"\n");
			set_name();
		}
		o_.out("Modify questions? Y/N\n");
		if (i_.YesNo()){ 
			
			while ( !done ){
				modify_question();
				o_.out("Modify another question? Y/N\n");
				if ( !i_.YesNo() ){done=true;}
			}
		}
		done=false;
		o_.out("Add questions? Y/N\n");
		if (i_.YesNo()){
			while ( !done){
				add_question();
				o_.out("add another question? Y/N\n");
				if ( !i_.YesNo() ){done=true;}
			}
		}
	}
	public Question modify_question(){
		o_.out("Modify which question?\n");
		int count=1;
		for(Question q:questions_){
			o_.out(count+": "+q.get_prompt()+"\n");
			count+=1;				
		}
		int inp=i_.IntInRange(1, count);
		Question q=questions_.get(inp-1);
		q.modify();
		return q;
		
	}
	public Survey(){}

	public void set_in(Input inp) {
		i_=inp;		
	}

}
