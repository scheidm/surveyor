import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;


public class Test extends Survey
{
	// Administer calls grade method automatically. grade also works with submit objects grade calls to re-grade
	private static final long serialVersionUID = -1660872660862755598L;
	private ArrayList<Response> key_;

	public Test(Output o, Input i) {
		super(o,i);
		key_=new ArrayList<Response>();
	}
	public void save(){
		try
	      {
			 FileOutputStream fileOut = 
	         new FileOutputStream(name_+".t.ser");
	         ObjectOutputStream out = new ObjectOutputStream(fileOut);
	         out.writeObject(this);
	         out.close();
	          fileOut.close();
	      }catch(IOException i)
	      {
	          i.printStackTrace();
	      }
	}
	public void display()//menus self-contained, displays then returns to menu
	{
		o_.out(name_+"\n");
		for(int count=0;count<(questions_.size());count++){
			Question q=questions_.get(count);
			q.display();
			o_.out( "Correct answer:\n" );
			q.showCorrect();
			o_.out("\n\n");
		}
		o_.out("\n");
	}
	public void grade( Submit s ){
		int count=0;
		int correct=0;
		int ungraded=0;
		ArrayList<Response> user=s.get_responses();
		for (Question q:questions_){
			Response key=q.get_correct();
			if (key.get_graded()){
				if(user.get(count).match(key)){
					correct+=1;
				}
			}
			else{ungraded+=1;}
			count+=1;
		}
		double pointperq = 100/questions_.size();
		double max = (questions_.size()-ungraded)*pointperq;
		o_.out("Submission Graded\nScore:"+(correct*pointperq)+"/"+max+"\n");
	}
	public void grade(){
		for (Submit s: submissions_){
			grade(s);
		}
		o_.out("\n");
		menu();
	}
	public void menu(){
		o_.out(name_+" main menu\n");
		o_.out("-1: Exit without saving\n 0: Save and exit\n");
		o_.out(" 1: Display\n 2: Modify\n");
		o_.out(" 3: Administer\n 4: Display responses\n");
		o_.out(" 5: Tabulate\n 6: Print Test\n");
		o_.out(" 7: Grade\n");
		
		int r = i_.IntInRange(-1, 7);
		switch(r){
		case 7: grade();
				break;
		case 6:
				printme(400);
				break;
		case 5:
				tabulate();
				break;
		case 4:
			view_responses();
			break;
		case 3: administer();
			break;
		case 2: modify();
			this.menu();
			break;
		case 1:	this.display();
			this.menu();
			break;
		case 0: this.save(); return;
		case -1: return;
		}
	}
	
	public ArrayList<Response> get_key()
	{
		return key_;
	}
	public void set_key(ArrayList<Response> l)
	{
		key_=l;
	}
	
	public void add_question(){
		super.add_question();
		Question last=questions_.get(questions_.size()-1);
		last.set_correct(last.response());
		

	}
	public Question modify_question(){
		Question q=super.modify_question();
		o_.out("Modify answer? Y/N\n");
		if (i_.YesNo()){q.set_correct(q.response());}
		return q;
	}
		
	
	
}
