public class BoolQ extends MultiChoice
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3130923530571973617L;
	public BoolQ(Output o, Input i)
	{
		o_=o;
		i_=i;
		prompt_="";
		StringOpt t=new StringOpt(o);
		t.set_text("True");
		StringOpt f=new StringOpt(o);
		f.set_text("False");
		options_.add(t);
		options_.add(f);	
	}
	public void display(){
		o_.out("True or false?\n");
		o_.out(prompt_+"\n");
		Integer c=1;
		for( Option i:options_){
			o_.out(c.toString()+": ");
			i.display(o_);
			o_.out("\n");
			c+=1;
		}
	}
	
	public void build(){set_prompt();}
	
	public Response response(){
		display();
		Response r=new Response(i_);
		Integer inp=0;
		inp=i_.IntInRange(1, 2);
		r.add_option(options_.get(inp-1));
		return r;
	}
	
	public void modify(){
		o_.out("Modify prompt? Y/N\n");
		if (i_.YesNo()){
			o_.out("Current Prompt:\n");
			o_.out(prompt_+"\n");
			set_prompt();
			
		}
	}

}
