public class Point extends Feature {

	private int x;
	private int y;
	
	public Point(Renderer rend, int x, int y){
		this.x=x;
		this.y=y;
		this.gfx=rend;
	}
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	@Override
	public void translate(int dx, int dy) {
		// TODO Auto-generated method stub

	}

	@Override
	public void render() {
		gfx.drawPoint(x, y);
	}

}
