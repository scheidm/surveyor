public abstract class Option implements java.io.Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4234665447342477622L;
	
	protected String textVal_;
	
	public String get_text() {return textVal_;}
	public void set_text(String s) {textVal_=s;}
	
	public void display(Output o)
	{
		o.out(textVal_);
	}
	public Option(Output o)
	{
		textVal_="";
	}
	public abstract void build(Input in, Output o);
	public Option(){}
	public abstract void modify(Input in, Output o);
	public abstract boolean match(Option o);
}