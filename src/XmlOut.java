import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


public class XmlOut extends Output {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2664209817508346348L;
	private FileOutputStream out;
	private int line;
	public String TEXT_HEAD="<text size=\"24\" x=\"20\" y=\"";
	public String TEXT_TAIL="</text>\n";
	
	public XmlOut(String s){
		try {
			out=new FileOutputStream(s);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		line=1;
	}
	
	public void header(int height){
		try {
			out.write( ("<geometry width=\"600\" height=\""+height+"\">\n").getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void footer(){
		try {
			out.write("</geometry>\n".getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void openText(){
		try {
			out.write( (TEXT_HEAD+27*line+"\">").getBytes() );
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public void closeText(){
		try {
			out.write( TEXT_TAIL.getBytes() );
		} catch (IOException e) {
			e.printStackTrace();
		}
		line+=1;
	}
	@Override
	public void out(String s) {
			boolean x=s.contains("\n");
			String c=s.replaceAll("\n","");
			try {
				out.write( c.getBytes() );
			} catch (IOException e) {
				e.printStackTrace();
			}
			if(x){
				closeText();
				openText();
			}
	}
	public void close(){
		try {
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void out(StringOpt s) {
		
		out(s.get_text());
	}

	@Override
	public void out(Integer i) {
			try {
				out.write(i.byteValue());
			} catch (IOException e) {
				e.printStackTrace();
			}
	}

	@Override
	public void out(char x) {
		try {
			out.write((byte)x);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
