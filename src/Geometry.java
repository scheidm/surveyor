import java.util.ArrayList;

public class Geometry {
	private ArrayList<Feature> features;
	private int width;
	private int height;
	
	public Geometry(int w, int h){
		this.width=w;
		this.height=h;
		this.features=new ArrayList<Feature>();
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
	
	public int count(){ return 0;}
	
	public void add(Feature f){ features.add(f);}
	
	public void render(){
		for (Feature f:features){
			f.render();
		}
	}
	
	public void iterator(){}

}
