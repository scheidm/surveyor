import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
//note: changing name does not remove old save
public class Surveyor
{

	public static void main(String[] args) throws IOException {
		Output pri = new Cout();
		Input in = new Cin( pri );
		menu(pri, in);
		//Export.main(args);
		//XmlOut test=new XmlOut("test.xml");
		//test.header();
		//test.footer();
				
	}
	public static void menu(Output o, Input i){//Main menu for program
		int x=0;
		while(x>=0){
			
			o.out("Surveyor Main Menu\n");
			o.out(" 0: Exit\n");
			o.out(" 1: Create Test\n 2: Create Survey\n");
			o.out(" 3: Load Test\n 4: Load Survey\n");
			x=i.IntInRange(0, 4);
			Boolean type=x%2==1;
			switch(x){
			case 0:return;
			case 1:
			case 2:	
				if (type==false){
					o.out("Create Survey\n");
					Survey make=new Survey(o, i);//create survey, modify also builds empty objects.
					make.build();
					make.menu();
				}
				else {
					o.out("Create Test\n");
					Test make= new Test(o,i);
					make.build();
					make.menu();
				}
				break;
			case 3:
			case 4:
				select(o, i, type);
				break;
			default: o.out("Invalid input, enter number from menu above\n");
				break;
			}
			if(x>0){x=0;}
		}
	}
	public static void select(Output o, Input i, Boolean type){
		
		
		File f= new File("./");
		Integer count=1;
		ArrayList<String> surveys=new ArrayList<String>();
		ArrayList<String> tests=new ArrayList<String>();
		for(String s:f.list()){
			if ( s.endsWith(".s.ser") ){
				surveys.add(s);
			}
			else if ( s.endsWith(".t.ser") ){
				tests.add(s);
			}
		}
		if(type==false){
			o.out("Select a survey from the list below\n");
			for (String s:surveys){
				o.out(count.toString()+": ");
				o.out(s.substring(0,s.length()-6));
				o.out("\n");
				count+=1;
			}
		}
		else {
			o.out("Select a test from the list below\n");
			for (String s:tests){
				o.out(count.toString()+": ");
				o.out(s.substring(0,s.length()-6));
				o.out("\n");
				count+=1;
			}
		}
		if (count>0){
			int r=i.IntInRange(1, count);
			if(type==false){
				Survey s=new Survey(o, i);
				s=load(surveys.get(r-1), o, i, s);
				s.menu();
			}
			else{
				Test s=new Test(o, i);
				s=load(tests.get(r-1), o, i, s);
				s.menu();
			}
			return;
		}
	}

	public static Survey load( String file, Output o, Input inp,Survey s )
	{
		try{
           FileInputStream fileIn = new FileInputStream(file);
           ObjectInputStream in = new ObjectInputStream(fileIn);
           s = (Survey)in.readObject();
           in.close();	
           fileIn.close();
       }catch(IOException i)
       {
           i.printStackTrace();
       }catch(ClassNotFoundException c)
       {
           System.out.println("Employee class not found");
           c.printStackTrace();
       }
		s.set_in(inp);
		ArrayList<Question> qs=s.get_questions();
		for(Question q:qs){
			q.set_in(inp);
		}
		return s;
	}
	public static Test load( String file, Output o, Input inp, Test s )
	{
		try
        {
           FileInputStream fileIn = new FileInputStream(file);
           ObjectInputStream in = new ObjectInputStream(fileIn);
           s = (Test)in.readObject();
           in.close();
           fileIn.close();
       }catch(IOException i)
       {
           i.printStackTrace();
       }catch(ClassNotFoundException c)
       {
           System.out.println("Employee class not found");
           c.printStackTrace();
       }
		s.set_in(inp);
		ArrayList<Question> qs=s.get_questions();
		for(Question q:qs){
			q.set_in(inp);
		}
		return s;
	}
	
}
