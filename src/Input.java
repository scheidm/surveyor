import java.util.ArrayList;


public abstract class Input{
	public abstract int inInt();
	public abstract String inStr();
	public abstract boolean YesNo();
	public abstract int IntInRange(int lo, int hi);
	public abstract boolean ValidInt(int lo, int hi);
	public abstract ArrayList<Option> matchX(ArrayList<Option> opts, boolean rank);
}
