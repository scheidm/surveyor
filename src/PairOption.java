public class PairOption extends Option
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8406942601367681256L;
	private Option left_;
	private Option right_;

	public void display(Output o){
		left_.display(o);
		o.out(", ");
		right_.display(o);
	}
	public Option get_right()
	{
		return right_;
	}
	public Option get_left()
	{
		return left_;
	}
	public void set_left(Option o)
	{
		left_=o;
	}
	public void set_right(Option o)
	{
		right_=o;
	}
	
	public PairOption(){
	}
	@Override
	public void build(Input in, Output o) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void modify(Input in, Output o) {
		// TODO Auto-generated method stub
		
	}
	
	public boolean match(Option o){
		PairOption p=(PairOption) o;
		Option l=p.get_left();
		Option r=p.get_right();
		return l.equals(left_)&&r.equals(right_);
	}

}